# Hangul-MAK

Hangul-MAK is an attempt to store Hangul syllabic blocks more efficiently by using a final state machine. It is designed to be used with unicode and it shall be possible for the state machine to switch between unicode and Hangul-MAK characters using special byte sequences.

## The name

"Hangul-MAK" means "Hangul, móó annerschd kodiert", which is saarlandish (a german dialect) for "Hangul, encoded differently".

# How to use it

This section is in development.

Hangul-MAK shall be designed as a simple program that reads from stdin and outputs on stdout so that it can be used together with other unix tools.
